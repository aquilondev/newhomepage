<?php

$nyelv=array(

	'menu_vizio'=>'VÍZIÓNK',
	'menu_termek'=>'TERMÉKEINK',
	'menu_megoldas'=>'EGYEDI MEGOLDÁSOK',
	'menu_video'=>'VIDEÓK',
	'menu_stat'=>'STATISZTIKA ÉS REFERENCIÁK',
	'menu_kapcsolat'=>'KAPCSOLAT',
    'menu_nyelv'=>'hu',

	'banner_mainTitle'=>'ONLINE SKILL DEVELOPMENT',
	'banner_subTitle'=>'for leaders and colleages',


	"vizio"=>"VÍZIONK",

	"vizio_1_maintitle"=>"How can skill development be done cost effectively and successfully for a large number of employees?",
	"vizio_1_subtitle"=>"",
	"vizio_1_text"=>"
		Our answer:
		<ul class='listStyle'>
			<li><p><b>ONLINE</b> skill development is a cost effective and time-saving solution</p></li>
			<li><p>Popular with all age groups</p></li>
			<li><p>It is a positive experience, so most of the employees think of it as a reward</li>
			<li><p>Based on our own game statistics and international benchmark, this is an outstandingly effective means of development</p></li>
		</ul>
	",
	"vizio_1_text_2"=>"
	<b><span class='kek'>Average rate of satisaction with our gamification-base solutions:</span></b><br>
	<span class='kek duplaMeret'>over 80%</span>
	",
	"vizio_1_text_3"=>"
	  (<b>Budapest Airport</b> commercial units, <b>Groupama Insurance</b> leaders and colleagues, all Hungarian employees of a foreign bank, <b>Kongsberg</b> project engineers, <b>Príma Energia</b> leaders, <b>Knorr-Bremse</b> engineers, a <b>Scandinavian IT/Telekom</b> company's employees, a very prestigous <b>automotive company's</b> employees)
	",
	
	"vizio_2_maintitle"=>"The new generation of online trainings",
	"vizio_2_subtitle"=>"serious game",
	"vizio_2_text"=>"
		<ul class='listStyle'>
			<li><p>A serious game, or applied game develops skills through simulated real-life situations</p></li>
			<li><p>The game's story and motivational dynamics ensure the interested and involvement of players</p></li>
			<li><p>We use gamification based motivational tools</p></li>
			<li><p>The applied game is played in multiple parts, takes 2-3 weeks, 10-15 minutes daily</p></li>
		</ul>
	",

	"vizio_3_maintitle"=>"Every generation likes it and learns<br> efficently width it",
	"vizio_3_subtitle"=>"",
	"vizio_3_text"=>"
		<ul class='listStyle'>
			<li><p>We can achieve very good result with applied games. (The best solution is combining them with some training days.)</p></li>
			<li><p>The employees don't need to be taken out of work for days, they just have to be given the tima daily, to spend it on the programme.</p></li>
			<li><p>We have completed programs, and we make unique applied games for companies specifically, in English and in Hungarian as well.</p></li>
		</ul>
	",
	
	"vizio_4_maintitle"=>"How does it work?",
	"vizio_4_subtitle"=>"",
	"vizio_4_text"=>"
	<ul class='listStyle'>
		<li><p>We send e-mails daily, with a link, which directs the participant to the next part of the program. There are no access codes.</p></li>
		<li><p>The program runs for 10-15 days and takes 10-15 minutes daily.<br>Gamification bas3d games, which run for a longer period of time in smaller units are a highly effective and enjoyable way of development.</p></li>
		<li><p>We offer statistics info pages, with an access for HR professionals and leaders, and offer continuous IT support, with a friendly and competent professional team.</p></li>
	</ul>
	",
	


	'erdekel'=>'Igen, érdekel',
	"termek_maintitle"=>'TERMÉKEINK',
	"termek_subtitle"=>'Kész programjaink és egyedi igényekre<br>fejlesztett megoldásaink is vannak',
	"termek_1_maintitle"=>"PRÓBAREPÜLÉS",
	"termek_1_subtitle"=>"Teljesítménymenedzsment",
	"termek_1_content_short"=>"A <strong>„Léghajósok”</strong> és a <strong>„Próbarepülés”</strong> teljesítménymenedzsmentet támogató online eszközök.<br><br> 
         A TM folyamatot egy léghajós felkészítés keretében kísérjük végig. A ”Léghajósok” az adott TM alapjait rakja le (a konkrét vállalatra szabjuk).<br><br>
         A „Próbarepülés” vezetői és munkatársi moduljai a célkijelölő, utánkövető és értékelő beszélgetésekhez – a TM működtetéséhez szükséges készségeket fejleszti.”",

	"termek_2_maintitle"=>"DISC",
	"termek_2_subtitle"=>"Viselkedés típusok New Orleansban",
	"termek_2_content_short"=>"New Orleansban kell jó kommunikációs készségünket bizonyítani egy kemény főnök keze alatt ebben a fejlesztő programban.<br><br>
            A DISC viselkedési stílusok alkalmazásával azonban minden rázósnak tűnő helyzetet megoldhatunk!<br>
            A játékból a DISC rendszerét nem ismerők elsajátíthatják a tudást, aki meg már tréningen is részt vett, nekik segít a beépülésben, alkalmazásban.<br>
            (már angolul is!)",

	"termek_3_maintitle"=>"ORIENT EXPRESSZ",
	"termek_3_subtitle"=>"Stresszmenedzsment",
	"termek_3_content_short"=>"A legendás Orient Express kiváló helyszíne egy stresszmentesítő utazásnak. Városról városra utazva a csodás kocsi kényelmében 
            gyakorlati módszereket tanulhatunk a stressz kezeléséhez. Persze, ez a világ sem tökéletes, valami furcsa titok lengi be ezt a kalandos utazást...",

	"termek_4_maintitle"=>"GIORGO VELA NYOMÁBAN",
	"termek_4_subtitle"=>"Vezetőknek",
	"termek_4_content_short"=>"A <strong>„Giorgo Vela  nyomában”</strong> programban a vezetés legfontosabb dimenzióival dolgozunk.<br><br>
         A négy önállóan is alkalmazható modul: Vezetői önismeret; A vezetés kapcsolati dimenziója; A vezetés feladat oldala; Csapatfejlesztés.<br><br>
         Ezt a programot már több száz, különböző szintű vezető játszotta.  Izgalmasnak, nagyon hasznosnak találták.",
		 
	"termek_5_maintitle"=>"TELJESÍTMÉNY DIALÓGUS",
	"termek_5_subtitle"=>"Modellezés",
	"termek_5_content_short"=>"A <strong>„Teljesítménydialógus modellezésben”</strong> konkrét teljesítmény- értékelő, követő és célkijelölő párbeszédben vesz részt a játékos munkatársi vagy vezetői szerepben.<br><br> 
         Ebben a szimulációban döntései befolyásolják a beszélgetés sikerességét, a megoldásban, megértésben Mentor segíti.”",

    "termek_6_maintitle"=>"TELEKI 130",
	"termek_6_subtitle"=>"Csoportos együttműködés",
	"termek_6_content_short"=>"Az együttműködés szimulációs játékunk ugyanúgy online eszköz, mégis a csoportos munkát és a csapatmechanizmusokat erősíti. Afrikában számos kaland közepette modellezünk valós kihívásokat, melyek kiválóan átültethetőek a napi gyakorlatba.",
	
	"termek_7_maintitle"=>"PÁRBESZÉD SZIMULÁCIÓK",
	"termek_7_subtitle"=>"DISC alapon",
	"termek_7_content_short"=>"A DISC gyakorló szituációk a már meglévő DISC stílus ismeretekre építve nyújt gyakorlási lehetőséget konkrét szituációkra, kommunikációs helyzetekre.",
    "termek_7_content_1"=>"A DISC párbeszéd szimulációt követően nagy biztonsággal mehetsz bele akár kényesebb kommunikációs helyzetekbe is.<br>",
	"termek_7_content_2"=>"A helyzetek több irányba ágaznak szét, de csak egy a helyes. Mentorunk azonban mindig készen áll, hogy segítsen megtalálni a megfelelő hangnemet, és a megfelelő stílust.<br>",
	"termek_7_content_3"=>"Gyere, játssz, gyakorolj, hogy komfortosabb és magabiztosabb legyél minden viselkedés stílussal történő kommunikáció során.",	


	'stat_ref'=>'Statisztika és referenciák',

	'ered_fej1'=>'Our results:',
	'ered_fej2'=>'Groupama Insurance - cultural change',
	'ered_title1'=>'8 training days, 8 weeks of gamificaion for leaders - out of 178 leader 156 was active<br>2 weeks of gamification for the whole company - out of 2000 colleague 1147 was active',
	'ered_title2'=>'The following figure shows the opinion of the participants before and after the program, (online questionnaire)',

	'ered_graf_t1'=>'Good atmosphere',
	'ered_graf_t2'=>'Individual effort',
	'ered_graf_t3'=>'Willingness for co-operation',
	'ered_graf_t4'=>'Participation, collabration',
	'ered_graf_t5'=>'Integrated organizational goals',
	'ered_graf_t6'=>'Common language',
	'ered_graf_t7'=>'Client-orientation',
	'ered_graf_t8'=>'Total avarege',
	'ered_graf_t9'=>'on average:',
	'ered_graf_t10'=>'Result before the program',
	'ered_graf_t11'=>'Result after the program',


	'bud_fej1'=>'Our result:',
	'bud_fej2'=>'Budapest Airport - Behavioural change - commercial and hosting units',
	'bud_t_1'=>'Needs of the client:',
	'bud_t_2'=>'Preliminary assessment, behaviour observation based mystery shopping',
	'bud_t_3'=>'1 day of training',
	'bud_t_4'=>'400+ participants',
	'bud_t_5'=>'2x2 weeks of gamification',
	'bud_t_6'=>'Closing assessment, result',
	'bud_t_7'=>'In commercial units twice as many people showed the expected behaviour 6 months after the program, than before it.<br>
				<span class="kek"><strong>We got these result in spite of a fluctuation of 40%!</strong></span>',
	'bud_t_8'=>'before',
	'bud_t_9'=>'after',


	'vissza_fej'=>'Feedback',
	'vissza_velemeny_1'=>'"The first results appeared after around 6 months at the middle management level, in the quality of the management meetings, the huge improvement of trust and cooperation needed in the various projrcts in force in the company.<br>
	I would add also that during the last quarter of last year we improved much more than expected the business for the first time since October 2008 when I joined Hungary. Something happened, started to change progressively."',
	'vissza_iro_1'=>'(Yann Ménetrier, Groupama insurance, CEO)',

	'vissza_velemeny_2'=>'"Yes! It was worth the effort! Because it was entertaining it was illuminating(because time to time I had to stop and examine myself), and it was developing (because I got into close contact with excellent colleagues).<br>
                            Thank you for all, and I hope we see each other soon!"',
	'vissza_velemeny_3'=>'"An adventurous, full of experiences journey has ended. A journey like this must be a fantastic experience in real life. It was worth joining for this couple of weeks. Ilearned a lot from the tasks, and the videos were very instructive as well.<br>
                            Thank you very much.<br>I wish all the best to your team!"',

    'kapcs_nev'=>'AQUILONE TRAINING KFT.',
    'kapcs_eler'=>'1145 Budapest, Amerikai út 76.<br><br>Tel: 06 70 428 2360<br>',


	"egyedi_maintitle"=>"Egyedi megoldások",
	"egyedi_text"=>"
	<ul class='listStyle'>
		<li><p>Kultúraváltást támogató játék<br><i>Groupama Insurance</i></p></li>
		<li><p>Értékesítés (Alapok + upselling) támogató játékok<br><i>Budapest Airport commercial units</i></p></li>
		<li><p>Teljesítménymenedzsment<br><i>Külföldi tulajdonú Bank</i></p></li>
		<li><p>Együttműködést fejlesztő komplex játék<br><i>Scandinavian IT/Telekom company</i></p></li>
	</ul>
	",

	"video_maintitle"=>"Videók",





	"close"=>"11 days to go"
);

?>

