<?php

$nyelv=array(

	'menu_vizio'=>'VÍZIÓNK',
	'menu_termek'=>'TERMÉKEINK',
	'menu_megoldas'=>'EGYEDI MEGOLDÁSOK',
	'menu_video'=>'VIDEÓK',
	'menu_stat'=>'STATISZTIKA ÉS REFERENCIÁK',
	'menu_kapcsolat'=>'KAPCSOLAT',
	'menu_nyelv'=>'en',

	'banner_mainTitle'=>'ONLINE KÉSZSÉGFEJLESZTÉS',
	'banner_subTitle'=>'vezetőknek, munkatársaknak',

	'vissza'=>"Vissza",

	"vizio"=>"VÍZIÓNK",

	"vizio_1_maintitle"=>"Hogyan lehet - akár nagylétszámú - munkatárs készségfejlesztését",
	"vizio_1_subtitle"=>"költséghatékonyan és sikerrel megoldani?",
	"vizio_1_text"=>"
		Válaszunk:
		<ul class='listStyle'>
			<li><p><b>ONLINE</b> készségfejlesztés <b>költséghatékony és időtakarékos</b> megoldás.</p></li>
			<li><p><b>Népszerű</b> a fiatalabb generáció körében és az idősebbek is kedvelik.</p></li>
			<li><p><b>Jutalomként</b> éli meg a legtöbb munkavállaló a <b>pozitív élmény</b> miatt.</p></li>
			<li><p>Saját <b>méréseink</b> és nemzetközi benchmark-ok alapján is a <b>leghatékonyabb</b> fejlesztési eszköz.</p></li>
		</ul>
	",
	"vizio_1_text_2"=>"
	<b><span class='kek'>Átlagos elégedettség</span> gamification alapúmegoldásainkkal:</b><br>
	<span class='kek duplaMeret'>80% felett</span>
	",
	"vizio_1_text_3"=>"
	  (<b>Budapest Airport</b> kereskedelmi egységei, <b>Groupama Biztosító</b> vezetői és munkatársai, külföldi tulajdonú <b>Bank</b> teljes munkavállalói köre, <b>Kongsberg</b> projektmérnökei, <b>Príma Energia</b> vezetői, <b>Knorr-Bremse</b> mérnökei, <b>nagy informatikai</b> vállalat munkatársai, <b>legnagyobb autóipari</b> vállalat munkatársai.)
	",
	
	"vizio_2_maintitle"=>"Az online tréningek új generációja",
	"vizio_2_subtitle"=>"a serious game",
	"vizio_2_text"=>"
		<ul class='listStyle'>
			<li><p>A serious game, vagy alkalmazott játék <b>valós helyzeteket szimulál</b>, és ezeken keresztül fejleszt keszséget.</p></li>
			<li><p>Az érdeklődést és bevonódást a játék <b>történetbe ágyazottsága</b> és motivációs dinamikája hozza létre.</p></li>
			<li><p>A fejlesztés <b>gamification-re épül</b>, ennek motivációs eszközeit használjuk.</p></li>
			<li><p>Az alkalmazott játékot <b>több részletben</b> játssza le a résztvevő, 2-3 hétig <b>naponta 10-15 percet</b> foglalkozik a témával.</p></li>
		</ul>
	",

	"vizio_3_maintitle"=>"Minden generáció szereti és",
	"vizio_3_subtitle"=>"eredményesen tanul vele",
	"vizio_3_text"=>"
		<ul class='listStyle'>
			<li><p>Az alkalmazott játékkal a <b>tréningeknél jobb eredménnyel</b> tudunk dolgozni, a tudás valóban beépül.</p></li>
			<li><p>A munkatársakat <b>nem kell napokra kivenni a munkából</b>, termet bérelni, mindössze biztosítani kell számukra, hogy a programmal foglalkozzanak.</p></li>
			<li><p><b>Kész programjaink</b> és egyedi, <b>cégre fejlesztett</b> megoldásaink is vannak, angolul és magyarul.</p></li>
		</ul>
	",
	
	"vizio_4_maintitle"=>"Hogyan működik?",
	"vizio_4_subtitle"=>"",
	"vizio_4_text"=>"
	<ul class='listStyle'>
		<li><p>Napi e-mail <b>behívó üzenet</b>, nem szükséges belépési kódok alkalmazása. A levélben található <b>linkre kattintva</b> máris a program következő részében van a résztvevő.</p></li>
		<li><p>10-15 munkanapon napi <b>10-15 perc</b> figyelmet igényel a program. A kisebb egységekben hosszabb időn át futó gamification alapú kiemelkedően <b>hatékony és örömteli</b> módja a fejlesztésnek.</p></li>
		<li><p>A HR és a vezetők számára nyomon követhető <b>statisztikai felület</b>, folyamatos <b>IT támogatással</b>, barátságos és <b>hozzáértő szakmai team-mel.</b></p></li>
	</ul>
	",
	


	'erdekel'=>'Igen, érdekel',
	"termek_maintitle"=>'TERMÉKEINK',
	"termek_subtitle"=>'Kész programjaink és egyedi igényekre<br>fejlesztett megoldásaink is vannak',
	"termek_1_maintitle"=>"PRÓBAREPÜLÉS",
	"termek_1_subtitle"=>"Teljesítménymenedzsment",
	"termek_1_content_short"=>"A <strong>„Léghajósok”</strong> és a <strong>„Próbarepülés”</strong> teljesítménymenedzsmentet támogató online eszközök.<br><br> 
         A TM folyamatot egy léghajós felkészítés keretében kísérjük végig. A ”Léghajósok” az adott TM alapjait rakja le (a konkrét vállalatra szabjuk).<br><br>
         A „Próbarepülés” vezetői és munkatársi moduljai a célkijelölő, utánkövető és értékelő beszélgetésekhez – a TM működtetéséhez szükséges készségeket fejleszti.”",

	"termek_2_maintitle"=>"DISC",
	"termek_2_subtitle"=>"Viselkedés típusok New Orleansban",
	"termek_2_content_short"=>"New Orleansban kell jó kommunikációs készségünket bizonyítani egy kemény főnök keze alatt ebben a fejlesztő programban.<br><br>
            A DISC viselkedési stílusok alkalmazásával azonban minden rázósnak tűnő helyzetet megoldhatunk!<br>
            A játékból a DISC rendszerét nem ismerők elsajátíthatják a tudást, aki meg már tréningen is részt vett, nekik segít a beépülésben, alkalmazásban.<br>
            (már angolul is!)",

	"termek_3_maintitle"=>"ORIENT EXPRESSZ",
	"termek_3_subtitle"=>"Stresszmenedzsment",
	"termek_3_content_short"=>"A legendás Orient Express kiváló helyszíne egy stresszmentesítő utazásnak. Városról városra utazva a csodás kocsi kényelmében 
            gyakorlati módszereket tanulhatunk a stressz kezeléséhez. Persze, ez a világ sem tökéletes, valami furcsa titok lengi be ezt a kalandos utazást...",

	"termek_4_maintitle"=>"GIORGO VELA NYOMÁBAN",
	"termek_4_subtitle"=>"Vezetőknek",
	"termek_4_content_short"=>"A <strong>„Giorgo Vela  nyomában”</strong> programban a vezetés legfontosabb dimenzióival dolgozunk.<br><br>
         A négy önállóan is alkalmazható modul: Vezetői önismeret; A vezetés kapcsolati dimenziója; A vezetés feladat oldala; Csapatfejlesztés.<br><br>
         Ezt a programot már több száz, különböző szintű vezető játszotta.  Izgalmasnak, nagyon hasznosnak találták.",
		 
	"termek_5_maintitle"=>"TELJESÍTMÉNY DIALÓGUS",
	"termek_5_subtitle"=>"Modellezés",
	"termek_5_content_short"=>"A <strong>„Teljesítménydialógus modellezésben”</strong> konkrét teljesítmény- értékelő, követő és célkijelölő párbeszédben vesz részt a játékos munkatársi vagy vezetői szerepben.<br><br> 
         Ebben a szimulációban döntései befolyásolják a beszélgetés sikerességét, a megoldásban, megértésben Mentor segíti.”",

    "termek_6_maintitle"=>"TELEKI 130",
	"termek_6_subtitle"=>"Csoportos együttműködés",
	"termek_6_content_short"=>"Az együttműködés szimulációs játékunk ugyanúgy online eszköz, mégis a csoportos munkát és a csapatmechanizmusokat erősíti. Afrikában számos kaland közepette modellezünk valós kihívásokat, melyek kiválóan átültethetőek a napi gyakorlatba.",
	
	"termek_7_maintitle"=>"PÁRBESZÉD SZIMULÁCIÓK",
	"termek_7_subtitle"=>"DISC alapon",
	"termek_7_content_short"=>"A DISC gyakorló szituációk a már meglévő DISC stílus ismeretekre építve nyújt gyakorlási lehetőséget konkrét szituációkra, kommunikációs helyzetekre.",
    "termek_7_content_1"=>"A DISC párbeszéd szimulációt követően nagy biztonsággal mehetsz bele akár kényesebb kommunikációs helyzetekbe is.<br>",
	"termek_7_content_2"=>"A helyzetek több irányba ágaznak szét, de csak egy a helyes. Mentorunk azonban mindig készen áll, hogy segítsen megtalálni a megfelelő hangnemet, és a megfelelő stílust.<br>",
	"termek_7_content_3"=>"Gyere, játssz, gyakorolj, hogy komfortosabb és magabiztosabb legyél minden viselkedés stílussal történő kommunikáció során.",	


	'stat_ref'=>'Statisztika és referenciák',

	'ered_fej1'=>'Eredményeink:',
	'ered_fej2'=>'Groupama Biztosító - kultúraváltás',
	'ered_title1'=>'8 tréningnap, 8 hét gamification vezetőknek - 178 vezetőből 156 aktív<br>2 hét gamification teljes munkatársi körnek 2000 munkavállalóból 1147 aktív',
	'ered_title2'=>'Az alábbi ábra a résztvevők véleményét mutatja a program előtt, és utána. (Online mérés)',

	'ered_graf_t1'=>'Jó atmoszféra',
	'ered_graf_t2'=>'Egyéni erőfeszítés',
	'ered_graf_t3'=>'Együttműködési készség',
	'ered_graf_t4'=>'Részvétel, együttműködés',
	'ered_graf_t5'=>'Szervezeti célok integráltak',
	'ered_graf_t6'=>'Közös nyelv',
	'ered_graf_t7'=>'Ügyfélorientáció',
	'ered_graf_t8'=>'Teljes átlag',
	'ered_graf_t9'=>'Átlagosan:',
	'ered_graf_t10'=>'Program előtti eredmény',
	'ered_graf_t11'=>'Program utáni eredmény',


	'bud_fej1'=>'Eredményeink:',
	'bud_fej2'=>'Budapest Airport - Viselkedés változás - kereskedelmi<br> és vendéglátó egységek',
	'bud_t_1'=>'Ügyféligény:',
	'bud_t_2'=>'Előzetes mérés, viselkedés-megfigyeléses próbavásárlással',
	'bud_t_3'=>'1 nap tréning',
	'bud_t_4'=>'400+ résztvevő',
	'bud_t_5'=>'2x2 hét gamification',
	'bud_t_6'=>'Záró mérés, lekövethető eredmények',
	'bud_t_7'=>'A kereskedelmi egységek dolgozói közül a program után fél évvel majd kétszerannyian mutatták az elvárt viselkedéseket, mint a program előtt.<br>
				<span class="kek"><strong>Mindezt 40%-os fluktuáció mellett!</strong></span>',
	'bud_t_8'=>'Előtte',
	'bud_t_9'=>'Utána',


	'vissza_fej'=>'Visszajelzések',
	'vissza_velemeny_1'=>'"Az első eredmények nagyjából 6 hónap elteltével jelentkeztek a középvezetői szinten - ez megnyilvánult a vezetői értekezletek minőségében, továbbá jelentősen megnövekedett a bizalom és az együttműködés a vállalatban futó különböző projektekben.<br>
			Még azt is hozzátenném, hogy az év utolsó negyedévében a vártnál sokkal jobban nőttünk az üzletben -2008. októberi csatlakozásom óta először."',
	'vissza_iro_1'=>'(Yann Ménetrier, Groupama Biztosító vezérigazgató)',

	'vissza_velemeny_2'=>'"Igen! Megérte az erőfeszítést!<br>
			Mert szórakoztató volt, megvilágosító volt (mert időről-időre meg kellett állnom és magamba kellett néznem), és igen fejlesztő volt (mert közeli kapcsolatba kerültem igazán kiválló kollégákkal).<br>
			Köszönet mindenért, és remélem hamarosan újra látjuk egymást!"',
	'vissza_velemeny_3'=>'"A kalandos tapasztalatokban bővelkedő utazásnak vége. Egy ilyen utazás a valóságban biztos fantasztikus élmény. Megérte csatlakozni erre a pár hétre. Sokat tanultam a feladatokból, és a videókból amelyek szintén hasznosak és érdekesek voltak.<br><br>
			Nagyon köszönöm!<br>Minden jót kívánok az egész Aquilone csapatnak!"',

    'kapcs_nev'=>'AQUILONE TRAINING KFT.',
    'kapcs_eler'=>'1145 Budapest, Amerikai út 76.<br><br>Tel: 06 70 428 2360<br>',


	"egyedi_maintitle"=>"Egyedi megoldások",
	"egyedi_text"=>"
	<ul class='listStyle'>
		<li><p>Kultúraváltást támogató játék<br><i>Groupama Biztosító</i></p></li>
		<li><p>Értékesítés (Alapok + upselling) támogató játékok<br><i>Budapest Airport kereskedelmi egységek</i></p></li>
		<li><p>Teljesítménymenedzsment<br><i>Külföldi tulajdonú Bank</i></p></li>
		<li><p>Együttműködést fejlesztő komplex játék<br><i>multinacionális IT/telekom vállalat</i></p></li>
	</ul>
	",

	"video_maintitle"=>"Videók",

	'ballon'=>array(
		'A teljesítménymenedzsmentet támogató programunkat egy kalandos hőlégballonos utazás analógiájára építettük. Földi és légi oktatáson veszünk részt. A földi oktatás (Léghajósok) az alapokat adja át, míg a légi oktatás (Próbarepülés) a TM beszélgetésekhez kapcsolódó készségfejlesztés.',
		'A tanulásban, azaz a „hőlégballonos felkészülésben” egy tapasztalt pilóta kísér. Minden nap megkér egy-egy feladat elvégzésére. Itt például a sikeresség külső tényezőiről és a saját erősségekről kell, hogy gondolkozz, és amit értékesnek találsz, azt szimbolikusan belehelyezheted a hőlégballon kosarába. Látni fogod azt is, mások milyen elemeket választottak a leggyakrabban.',
		'Néha egész rejtélyes helyekre kalandozunk. De csakis a jó ügy érdekében. A komfortzóna elhagyása, illetve játékelemekkel támogatott önreflexió pedig a tanulást segíti ☺',
		'A felkészülés során, minden jól megoldott feladatért egy, a léghajózáshoz nélkülözhetetlen eszközzel gyarapszol. A teljesítménymenedzsmenthez szükséges tudást pedig egy külön „jegyzetben” gyűjtöd, amit bármikor elővehetsz,  ha szükséges.  A jegyzet letölthető és tartalmazza az összes, a játékba beépített tudást, sűrített formában.',
		'És a végén, ha már mindent magtanultunk a teljesítménymenedzsmentről, akkor a magasba emelkedünk, hogy folytassuk a kalandot és az alapok után elsajátíthassuk a legfontosabb készségeket.',
		'A program során érintett témák:<br/><ul><li>A jó teljesítmény összetevői.</li><li>A teljesítmény iránya, fókusza és minősége.</li><li>A teljesítmény értékelő beszélgetés és forgatókönyve.</li><li>Szerepek és felelősségi körök a teljesítmény menedzsmentben.</li><li>A feladat és a kapcsolati oldal harmonikus működtetése a TM-ben.</li></ul>A program során felhalmozott eszköztár egyéb vezetői/munkatársi kommunikációs helyzetben is rendkívül hasznos lehet.<br/><br/>10+15 napban, napi 10-15 perc.<br/>Résztvevők száma korlátlan'
	),

    'disc' =>array(
    	'A DISC viselkedéstípusok programunk DISC tréning utánkövetésként, vagy önálló tananyagként is kiválóan használható. A történet New Orleansban játszódik, ahová négy idegen érkezik a „DISC minden irányából”. Az ő helyzeteikkel, ügyes bajos dolgaikkal találkozol Te, mint szállásadó. A feladat nem mindig egyszerű, de izgalmas és hasznos. Tarts velünk!',
		'Az első 4 napban a 4 viselkedés dimenziót elevenítjük meg neked, a domináns, a befolyásoló, a stabil és a szabálykövető viselkedések külső és nonverbális kommunikációs jegyeire építve. Egyedül itt sem maradsz, hiszen könyörtelen főnököd, Tim segít, ha elakadnál…',
		'Megismerkedhetünk a vendégek fő viselkedési dimenzióival, kommunikációs és motivációs igényeivel, jellemzőivel.',
		'<span style="font-size: 80%;">A DISC viselkedéstípusok program során nemcsak gyakorlatok egymásutánjával találkozik a felhasználó, hanem egy teljes, részletes DISC kézikönyv is összeáll a háttérben. A program önálló modulként vagy<br/><ul><li>Kommunikáció</li><li>Együttműködés fejlesztés</li><li>Vezetésfejlesztés</li></ul>témákban szolgálhat kiegészítő modulként.<br/><br/>15 napban, napi 10-15 perc<br/>Résztvevők száma korlátlan.</span>'
	),

    'orient'=> array(
    	'Stresszkezelés programunkkal játékosaink Európa leghíresebb vasútján Párizstól Isztambulig utazva kapnak gyakorlati módszereket.',
    	'Tréfás és komoly, egyéni és csoportos feladatok várnak az utazókra.',
		'A stresszcsökkentő és figyelemkoncentrációs gyakorlatok  a játékból egyszerűen átültethetőek a valódi életbe. Így buzdítjuk is a felhasználókat, hogy álljanak fel a gép elől és mozogjanak egy kicsit. Ez jó módszer egy hosszú vonatúton is.',
		'Dolgozunk az objektív és a szubjektív idővel és tippeket adunk arra, hogy a saját időbeosztásunk hasznosan teljen. Tervezés-újratervezés-szervezés. ☺',
		'Programunkat ajánljuk mindazoknak, akik nagy időnyomás alatt és az ebből fakadó komoly fizikai, szellemi és pszichikai terhelésnek vannak kitéve.<br/><br/>15 napban, napi 10-15 perc<br/>Résztvevők száma korlátlan'
	),

    'giorgio'=> array(
    	'A „Giorgio Vela nyomában” vezetésfejlesztést támogató online program. A kaland során Marseilles-ből egészen egy eldugott Indiai-óceáni szigetre visz az utunk. Közben vezetői helyzetekben kell helytállnunk egy hajó kapitányaként.',
		'A legénység néha bizony szem elől téveszti a célokat és az irányt. Ezért jó, ha van egy kapitány, aki megerősíti és összetartja a csapatot.',
		'Nem elég azonban jó szakembernek lenni. Ismerni kell a munkatársak elvárásait, felismerni, megjegyzéseik, mondataik valójában miről szólnak... Az empátia és a kapcsolati oldal ápolása nélkülözhetetlen vezetői kompetencia. A figyelem és megfelelő értelmezés az alapja a támogató vezetői viselkedésnek.',
		'Útközben segítőink is akadnak. Ilyen Thomas kapitány Colombóban, akinek a vezetői motivációs modellje megkérdőjelezhetetlen és a nyugati világban is hasznos módszer.',
		'<span style="font-size: 85%;">Vezetésfejlesztő programunkat ajánljuk teljes vezetésfejlesztéshez illetve rész modulokhoz.<br/>4X10 napos, napi 15 percet igénylő modul:<br/><ul><li>Vezetői önismeret: a transzformális  vezetés, autoritás, felelősség, bizalom, kitartás, vezetői teszt</li><li>A vezetés kapcsolat és változás oldala: EQ, kapcsolatmenedzsment kompetenciák, kommunikáció, visszajelzés, változás kezelése feladat-kapcsolat dimenziókra építve.</li><li>Feladat dimenzió: feladatok kezelése, delegálás, időmenedzsment, projekt szemlélet, számonkérés</li> <li>Csapatépítés: Csapatfejlődés fázisai és a vezető feladatai, bizalom, felelősség, eredményesség, konfliktusok kezelése, önmenedzselő munkatársak – motiváló környezet</li></ul></span>'
	),

    'teljesitmeny'=> array(
    	'Játékunk erősségekre alapuló, a TM feladat-kapcsolat-változás dimenzióit figyelembe vevő megközelítésre épül. A teljesítménydialógus modellezésben valós teljesítményt értékelő, követő és célkijelölő párbeszédben vesz részt a játékos vezetői vagy munkatársi szerepben. ',
		'Ahogy a beszélgetés halad, a játékos döntéseket hoz, és ezeknek a döntéseknek következménye van, a másik szereplő ezekre valós reakciókat ad. A helyes megoldásra egy mentor ösztönzi, illetve ad visszajelzést akkor is, ha nem a megfelelő irányba indul a párbeszéd.',
		'A legfontosabb tudnivalók letölthetők a játékból.',
		'Minden nap végén kérünk véleményt a játékosainktól.<br/><br/>A játék statisztikáit egy felületen a HR szakemberek részére hozzáférhetővé tesszük, így folyamatosan követhetik munkatársaik haladását.'
	),

    'teleki'=> array(
    	'Minden nap az expedíciós sátorban kezdődik és ér véget. Itt kapod meg a feladatot és itt tudod ellenőrizni a pontjaidat, kipihenni az aznapi próbatételeket és nem utolsó sorban itt követheted nyomon a haladásod.',
		'A napi feladatok próbára teszik az együttműködő készséged és új ötletekkel, tanácsokkal látnak el a kommunikációs  helyzetek és feladatmegoldások terén.',
		'A kaland során a 130 évvel ezelőtti Teleki expedíció útját járod be. Ez a kaland egyedül veszélyes. Csak a társaid segítségével jutsz előre és érsz el a célig.'
	),

    'discdialog'=> array(
    	'A DISC párbeszéd szimulációt követően nagy biztonsággal mehetsz bele akár kényesebb kommunikációs helyzetekbe is.',
		'A helyzetek több irányba ágaznak szét, de csak egy a helyes. Mentorunk azonban mindig készen áll, hogy segítsen megtalálni a megfelelő hangnemet, és a megfelelő stílust.',
		'Gyere, játssz, gyakorolj, hogy komfortosabb és magabiztosabb legyél minden viselkedés stílussal történő kommunikáció során.'
	),




	"close"=>"11 days to go"
);

?>

