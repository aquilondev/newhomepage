<div id="fejezetKapcsolat">
    <div class="container">
        <div class="text_center">
            <?php include 'menuItem.php'; ?>
        </div>
        <div id="footer">
            <img src="image/aquilone.png" id="aquLogo">
            <div id="kapcsInfo">
                <span class="title"><?=$nyelv["kapcs_nev"]?></span><br>
                <?=$nyelv["kapcs_eler"]?>
            </div>
            <div id="kozosseg">
                <a href="https://www.facebook.com/Aquilone-Training-Kft-139879789391656/?fref=ts" target="_blank"><img src="image/facebook.png" id="facebook"></a>
                <a href="mailto:info@aquilone.hu" target="_blank"><img src="image/mail.png" id="mail"></a>
            </div>
            <div class="clear"><br></div>
        </div>

    </div>
</div>
