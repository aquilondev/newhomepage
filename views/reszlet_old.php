<div class="reszletFedo" id="reszletTeleki">
    <div class="detailContainer" style="background-image: url(image/slides/teleki/teleki_bkg.jpg);">
        <div class="buttonContainer">
            <div class="detailButton left"></div>
            <div class="detailButton right"></div>
            <div class="detailButtonBack"><?=$nyelv["vissza"]?></div>
        </div>
        <div class="detailWrap">
            <div class="detailItems" tema="teleki">
                <div class="detailItem">
                    <img src="image/slides/teleki/1.jpg" class="picture">
                    <div class="description"><?=$nyelv['teleki'][0]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/teleki/2.jpg" class="picture">
                    <div class="description"><?=$nyelv['teleki'][1]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/teleki/3.jpg" class="picture">
                    <div class="description"><?=$nyelv['teleki'][2]?></div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="reszletFedo" id="reszletDialog">
    <div class="detailContainer" style="background-image: url(image/slides/teljesitmeny/teljesitmeny_bkg.jpg);">
        <div class="buttonContainer">
            <div class="detailButton left"></div>
            <div class="detailButton right"></div>
            <div class="detailButtonBack"><?=$nyelv["vissza"]?></div>
        </div>
        <div class="detailWrap">
            <div class="detailItems" tema="teljesitmeny">
                <div class="detailItem">
                    <img src="image/slides/teljesitmeny/1.jpg" class="picture">
                    <div class="description"><?=$nyelv['teljesitmeny'][0]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/teljesitmeny/2_k.png" class="picture screen">
                    <img src="image/slides/teljesitmeny/2_nk.jpg" class="picture mobile">
                    <div class="description"><?=$nyelv['teljesitmeny'][1]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/teljesitmeny/3_k.png" class="picture screen">
                    <img src="image/slides/teljesitmeny/3_nk.jpg" class="picture mobile">
                    <div class="description"><?=$nyelv['teljesitmeny'][2]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/teljesitmeny/4_k.png" class="picture screen">
                    <img src="image/slides/teljesitmeny/4_nk.jpg" class="picture mobile">
                    <div class="description"><?=$nyelv['teljesitmeny'][3]?></div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="reszletFedo" id="reszletDISC_Dialog">
    <div class="detailContainer" style="background-image: url(image/slides/discdialog/discdialog_bkg.jpg);">
        <div class="buttonContainer">
            <div class="detailButton left"></div>
            <div class="detailButton right"></div>
            <div class="detailButtonBack"><?=$nyelv["vissza"]?></div>
        </div>
        <div class="detailWrap">
            <div class="detailItems" tema="teleki">
                <div class="detailItem">
                    <img src="image/slides/discdialog/1.jpg" class="picture">
                    <div class="description"><?=$nyelv['discdialog'][0]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/discdialog/2.jpg" class="picture">
                    <div class="description"><?=$nyelv['discdialog'][1]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/discdialog/3.jpg" class="picture">
                    <div class="description"><?=$nyelv['discdialog'][2]?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="reszletFedo" id="reszletVela">
    <div class="detailContainer" style="background-image: url(image/slides/giorgio/giorgio_bkg.jpg);">
        <div class="buttonContainer">
            <div class="detailButton left"></div>
            <div class="detailButton right"></div>
            <div class="detailButtonBack"><?=$nyelv["vissza"]?></div>
        </div>
        <div class="detailWrap">
            <div class="detailItems" tema="vela">
                <div class="detailItem">
                    <img src="image/slides/giorgio/1.jpg" class="picture">
                    <div class="description"><?=$nyelv['vela'][0]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/giorgio/2.jpg" class="picture">
                    <div class="description"><?=$nyelv['vela'][1]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/giorgio/3.jpg" class="picture">
                    <div class="description"><?=$nyelv['vela'][2]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/giorgio/4.jpg" class="picture">
                    <div class="description"><?=$nyelv['vela'][3]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/giorgio/5.jpg" class="picture">
                    <div class="description"><?=$nyelv['vela'][4]?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="reszletFedo" id="reszletOrient">
    <div class="detailContainer" style="background-image: url(image/slides/orient/orient_bkg.jpg);">
        <div class="buttonContainer">
            <div class="detailButton left"></div>
            <div class="detailButton right"></div>
            <div class="detailButtonBack"><?=$nyelv["vissza"]?></div>
        </div>
        <div class="detailWrap">
            <div class="detailItems" tema="orient">
                <div class="detailItem">
                    <img src="image/slides/orient/1.jpg" class="picture">
                    <div class="description"><?=$nyelv['orient'][0]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/orient/2_k.png" class="picture screen">
                    <img src="image/slides/orient/2_nk.jpg" class="picture mobile">
                    <div class="description"><?=$nyelv['orient'][1]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/orient/3_k.png" class="picture screen">
                    <img src="image/slides/orient/3_nk.jpg" class="picture mobile">
                    <div class="description"><?=$nyelv['orient'][2]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/orient/4_k.png" class="picture screen">
                    <img src="image/slides/orient/4_nk.jpg" class="picture mobile">
                    <div class="description"><?=$nyelv['orient'][3]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/orient/5.jpg" class="picture">
                    <div class="description"><?=$nyelv['orient'][4]?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="reszletFedo" id="reszletDISC">
    <div class="detailContainer" style="background-image: url(image/slides/disc/disc_bkg.jpg);">
        <div class="buttonContainer">
            <div class="detailButton left"></div>
            <div class="detailButton right"></div>
            <div class="detailButtonBack"><?=$nyelv["vissza"]?></div>
        </div>
        <div class="detailWrap">
            <div class="detailItems" tema="orient">
                <div class="detailItem">
                    <img src="image/slides/disc/1.jpg" class="picture">
                    <div class="description"><?=$nyelv['disc'][0]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/disc/2.jpg" class="picture">
                    <div class="description"><?=$nyelv['disc'][1]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/disc/3.jpg" class="picture">
                    <div class="description"><?=$nyelv['disc'][2]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/disc/4.jpg" class="picture">
                    <div class="description"><?=$nyelv['disc'][3]?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="reszletFedo" id="reszletProbarepules">
    <div class="detailContainer" style="background-image: url(image/slides/ballon/ballon_bkg.jpg);">
        <div class="buttonContainer">
            <div class="detailButton left"></div>
            <div class="detailButton right"></div>
            <div class="detailButtonBack"><?=$nyelv["vissza"]?></div>
        </div>
        <div class="detailWrap">
            <div class="detailItems" tema="orient">
                <div class="detailItem">
                    <img src="image/slides/ballon/1.jpg" class="picture">
                    <div class="description"><?=$nyelv['ballon'][0]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/ballon/2_k.png" class="picture screen">
                    <img src="image/slides/ballon/2_nk.jpg" class="picture mobile">
                    <div class="description"><?=$nyelv['ballon'][1]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/ballon/3_k.png" class="picture screen">
                    <img src="image/slides/ballon/3_nk.jpg" class="picture mobile">
                    <div class="description"><?=$nyelv['ballon'][2]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/ballon/4_k.png" class="picture screen">
                    <img src="image/slides/ballon/4_nk.jpg" class="picture mobile">
                    <div class="description"><?=$nyelv['ballon'][3]?></div>
                </div>
                <div class="detailItem">
                    <img src="image/slides/ballon/5_k.png" class="picture screen">
                    <img src="image/slides/ballon/5_nk.jpg" class="picture mobile">
                    <div class="description"><?=$nyelv['ballon'][4]?></div>
                </div>
            </div>
        </div>
    </div>
</div>
