<div id="fejezetEgyedi" class="fejezet">
    <div class="container">
        <h2 class="fontBlue"><?=$nyelv["egyedi_maintitle"]?></h2>
        <div class="vizioKep vizioBalra"><img src="image/egyediLaptop.png" class="kepSzelesseg"></div>
        <div class="vizioSzoveg vizioJobbra" id="egyediMegoldas"><?=$nyelv["egyedi_text"]?></div>
        <div class="clear vizioTerkoz"></div>
    </div>
</div>

<div id="fejezetVideo" class="fejezet">
    <div class="container" style="padding-top: 0px;">
        <h2 class="fontBlue"><?=$nyelv["video_maintitle"]?></h2>
        <div class="clear vizioTerkoz"></div>
        <div class="videoKeret">
            <video id="video_1" class="video" controls>
                <source src="media/Aquilone.mp4" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
        <div class="clear vizioTerkoz"></div>
        <div class="videoKeret">
            <video id="video_1" class="video" controls>
                <source src="media/Teleki_130.mp4" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
        <div class="clear vizioTerkoz"></div>
        <div class="videoKeret">
            <video id="video_1" class="video" controls>
                <source src="media/Baringo.mp4" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
        <div class="clear vizioTerkoz"></div>
    </div>
</div>
