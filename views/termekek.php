	<div id="fejezetTermek" class="fejezet">
		<div class="container">
			<h1><?=$nyelv["termek_maintitle"]?></h1>
			<h2><?=$nyelv["termek_subtitle"]?></h2>
			<div class="terkozFejlec"></div>
			<div id="Probarepules" class="termek paratlan">
				<div class="kep"></div>
				<h4 class="bold"><?=$nyelv["termek_1_maintitle"]?></h4>
				<h4><?=$nyelv["termek_1_subtitle"]?></h4>
				<div class="content"><?=$nyelv["termek_1_content_short"]?></div>
				<div class="gombErdekel"><?=$nyelv["erdekel"]?></div>
			</div>
			<div id="DISC" class="termek paros">
				<div class="kep"></div>
				<h4 class="bold"><?=$nyelv["termek_2_maintitle"]?></h4>
				<h4><?=$nyelv["termek_2_subtitle"]?></h4>
				<div class="content"><?=$nyelv["termek_2_content_short"]?></div>
				<div class="gombErdekel"><?=$nyelv["erdekel"]?></div>
			</div>
			<div class="clear"></div>
			<div id="Orient" class="termek paratlan">
				<div class="kep"></div>
				<h4 class="bold"><?=$nyelv["termek_3_maintitle"]?></h4>
				<h4><?=$nyelv["termek_3_subtitle"]?></h4>
				<div class="content"><?=$nyelv["termek_3_content_short"]?></div>
				<div class="gombErdekel"><?=$nyelv["erdekel"]?></div>
			</div>
			<div id="Vela" class="termek paros">
				<div class="kep"></div>
				<h4 class="bold"><?=$nyelv["termek_4_maintitle"]?></h4>
				<h4><?=$nyelv["termek_4_subtitle"]?></h4>
				<div class="content"><?=$nyelv["termek_4_content_short"]?></div>
				<div class="gombErdekel"><?=$nyelv["erdekel"]?></div>
			</div>
			<div class="clear"></div>
			<div id="Dialog" class="termek paratlan">
				<div class="kep"></div>
				<h4 class="bold"><?=$nyelv["termek_5_maintitle"]?></h4>
				<h4><?=$nyelv["termek_5_subtitle"]?></h4>
				<div class="content"><?=$nyelv["termek_5_content_short"]?></div>
				<div class="gombErdekel"><?=$nyelv["erdekel"]?></div>
			</div>
			<div id="Teleki" class="termek paros">
				<div class="kep"></div>
				<h4 class="bold"><?=$nyelv["termek_6_maintitle"]?></h4>
				<h4><?=$nyelv["termek_6_subtitle"]?></h4>
				<div class="content"><?=$nyelv["termek_6_content_short"]?></div>
				<div class="gombErdekel"><?=$nyelv["erdekel"]?></div>
			</div>	
			<div class="clear"></div>		
			<div id="DISC_Dialog" class="termek">
				<div class="kep"></div>
				<h4 class="bold"><?=$nyelv["termek_7_maintitle"]?></h4>
				<h4><?=$nyelv["termek_7_subtitle"]?></h4>
				<div class="content"><?=$nyelv["termek_7_content_short"]?></div>
				<div class="gombErdekel"><?=$nyelv["erdekel"]?></div>
			</div>
			<div class="clear"></div>
			
		</div>
	</div>
