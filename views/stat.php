<div id="fejezetStatRef" class="fejezet">
    <div class="container">
        <h2 class="fontBlue"><?=$nyelv["stat_ref"]?></h2>
        <div class="terkozFejlec"></div>
        <div id="groupama" class="stat_ref">
            <h2 class="fontBlue bold"><?=$nyelv["ered_fej1"]?></h2>
            <h2 class="bold"><?=$nyelv["ered_fej2"]?></h2>
            <div class="terkozFejlec" style="height: 2em"></div>
            <div class="text_center"><?=$nyelv["ered_title1"]?></div>
            <div class="terkozFejlec"></div>
            <div class="text_center"><?=$nyelv["ered_title2"]?></div>
            <div class="terkozFejlec"></div>
            <div id="GroupamaTabla">
                <div style="height: 0.8em"></div>
                <div class="GroupamaNew" style="width: 90%;"><span class="GroupamaCimke"><?=$nyelv["ered_graf_t1"]?></span><span class="GroupamaErtek">+20%</span></div>
                <div class="GroupamaOld bgBlue" style="width: 70%;"></div>
                <div class="GroupamaKoz"></div>
                <div class="GroupamaNew" style="width: 82%;"><span class="GroupamaCimke"><?=$nyelv["ered_graf_t2"]?></span><span class="GroupamaErtek">+37%</span></div>
                <div class="GroupamaOld bgBlue" style="width: 45%;"></div>
                <div class="GroupamaKoz"></div>
                <div class="GroupamaNew" style="width: 93%;"><span class="GroupamaCimke"><?=$nyelv["ered_graf_t3"]?></span><span class="GroupamaErtek">+65%</span></div>
                <div class="GroupamaOld bgBlue" style="width: 28%;"></div>
                <div class="GroupamaKoz"></div>
                <div class="GroupamaNew" style="width: 80%;"><span class="GroupamaCimke"><?=$nyelv["ered_graf_t4"]?></span><span class="GroupamaErtek">+50%</span></div>
                <div class="GroupamaOld bgBlue" style="width: 30%;"></div>
                <div class="GroupamaKoz"></div>
                <div class="GroupamaNew" style="width: 60%;"><span class="GroupamaCimke"><?=$nyelv["ered_graf_t5"]?></span><span class="GroupamaErtek">+40%</span></div>
                <div class="GroupamaOld bgBlue" style="width: 20%;"></div>
                <div class="GroupamaKoz"></div>
                <div class="GroupamaNew" style="width: 40%;"><span class="GroupamaCimke"><?=$nyelv["ered_graf_t6"]?></span><span class="GroupamaErtek">+10%</span></div>
                <div class="GroupamaOld bgBlue" style="width: 30%;"></div>
                <div class="GroupamaKoz"></div>
                <div class="GroupamaNew" style="width: 50%;"><span class="GroupamaCimke"><?=$nyelv["ered_graf_t7"]?></span><span class="GroupamaErtek">+50%</span></div>
                <div class="GroupamaKoz" style="width: 70%"><span class="GroupamaErtek"><?=$nyelv["ered_graf_t9"]?></span></div>
                <div class="GroupamaNew" style="width: 70%;"><span class="GroupamaCimke"><?=$nyelv["ered_graf_t8"]?></span><span class="GroupamaErtek">+39%</span></div>
                <div class="GroupamaOld bgBlue" style="width: 31%;"></div>
                <div class="GroupamaKoz"></div>
                <div style="position: absolute;bottom: -0.4em; left: -0.4em;box-sizing: border-box; height: .4em;width: .4em;border-right: 1px solid black;border-top: 1px solid black;"></div>
                <div class="GroupamaSegedvonal" style="left: 0;"></div>
                <div class="GroupamaSegedvonal" style="left: 10%;"></div>
                <div class="GroupamaSegedvonal" style="left: 20%;"></div>
                <div class="GroupamaSegedvonal" style="left: 30%;"></div>
                <div class="GroupamaSegedvonal" style="left: 40%;"></div>
                <div class="GroupamaSegedvonal" style="left: 50%;"></div>
                <div class="GroupamaSegedvonal" style="left: 60%;"></div>
                <div class="GroupamaSegedvonal" style="left: 70%;"></div>
                <div class="GroupamaSegedvonal" style="left: 80%;"></div>
                <div class="GroupamaSegedvonal" style="left: 90%;"></div>
                <div class="GroupamaKoord" style="left: 5%;">10%</div>
                <div class="GroupamaKoord" style="left: 15%;">20%</div>
                <div class="GroupamaKoord" style="left: 25%;">30%</div>
                <div class="GroupamaKoord" style="left: 35%;">40%</div>
                <div class="GroupamaKoord" style="left: 45%;">50%</div>
                <div class="GroupamaKoord" style="left: 55%;">60%</div>
                <div class="GroupamaKoord" style="left: 65%;">70%</div>
                <div class="GroupamaKoord" style="left: 75%;">80%</div>
                <div class="GroupamaKoord" style="left: 85%;">90%</div>
                <div class="GroupamaKoord" style="left: 95%;">100%</div>
            </div>
            <br><br><br><br>
            <div id="grafikonMagyarazGroupama">
                <div class="kockaElso"><div class="bgBlue magyarazoKocka">&nbsp;</div><?=$nyelv["ered_graf_t10"]?></div>
                <div class="kockaMasodik"><div class="bgGreen magyarazoKocka" >&nbsp;</div><?=$nyelv["ered_graf_t11"]?></div>
            </div>
            <br><br>
        </div>
        <div class="terkozFejlec"></div>
        <div id="BUDStat" class="stat_ref">
            <h2><strong><span class="kek"><?=$nyelv["bud_fej1"]?></span><br><?=$nyelv["bud_fej2"]?></strong></h2>
            <div class="terkozFejlec"></div>
            <div id="BUD_szoveg">
                <div style="height: 3em;"><?=$nyelv["bud_t_1"]?></div>
                <table width="100%">
                    <tr>
                        <td><img src="image/pont.png" class="pont"></td>
                        <td><?=$nyelv["bud_t_2"]?></td>
                    </tr>
                    <tr>
                        <td><img src="image/pont.png" class="pont"></td>
                        <td><?=$nyelv["bud_t_3"]?></td>
                    </tr>
                    <tr>
                        <td><img src="image/pont.png" class="pont"></td>
                        <td><?=$nyelv["bud_t_4"]?></td>
                    </tr>
                    <tr>
                        <td><img src="image/pont.png" class="pont"></td>
                        <td><?=$nyelv["bud_t_5"]?></td>
                    </tr>
                    <tr>
                        <td><img src="image/pont.png" class="pont"></td>
                        <td><?=$nyelv["bud_t_6"]?></td>
                    </tr>
                </table>
                <!--div class="clear"></div-->
                <div class="megjegy"><?=$nyelv["bud_t_7"]?></div>
            </div>
            <div id="BUD_grafikon">
                <div  style="height: 6.75em;width: 17em;margin-left: auto; margin-right: auto;">
                    <div style="height: 6.75em;width: 3.5em;float:left;">
                        <div style="font-size: 1.5em; padding: 1.6em 0px;line-height: 1em;">36%</div>
                    </div>
                    <div style="height: 6.75em;width: 6.75em;float:left;text-align: center;">
                        <img src="image/nyil1.png" style="height:6.75em;">
                    </div>
                    <div style="height: 6.75em;width: 6.75em;float:left;">
                        <div class="bold fontBlue" style="width: 3em;height: 1em; padding: 1em 0px; text-align: center; border-radius: 3.5em;font-size: 2.25em; line-height: 1em;background: white;">67%</div>
                    </div>
                </div>
                <div class="clear" style="height: 1em;"></div>
                <img src="image/grafikon.png" id="grafikonBUD">
                <div id="grafikonBeosztasBUD">
                    <div style="width: 6%;float: left;">&nbsp;</div>
                    <div class="beosztasBUD">20%</div>
                    <div class="beosztasBUD">40%</div>
                    <div class="beosztasBUD">60%</div>
                    <div class="beosztasBUD">70%</div>
                    <div class="beosztasBUD">100%</div>
                </div>
                <br><br>
                <div id="grafikonMagyarazBUD">
                    <div style="float: left;"> <div class="magyarazoKocka bgGrayMiddleDark">&nbsp;</div><?=$nyelv["bud_t_8"]?></div>
                    <div style="float: right;"><div class="magyarazoKocka bgBlue">&nbsp;</div><?=$nyelv["bud_t_9"]?></div>
                </div>
                <br><br>
                <div class="clear"></div>
            </div>
        </div>

        <div id="referencia" class="stat_ref">
            <h2 class="fontBlue bold"><?=$nyelv["vissza_fej"]?></h2>
            <div class="terkozFejlec"></div>
            <?=$nyelv["vissza_velemeny_1"]?><br>
            <span class="iro"><?=$nyelv["vissza_iro_1"]?></span>
            <div class="separator"></div>
            <?=$nyelv["vissza_velemeny_2"]?><br>
            <div class="separator"></div>
            <?=$nyelv["vissza_velemeny_3"]?><br>
        </div>
    </div>
</div>
