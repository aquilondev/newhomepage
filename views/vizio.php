<div id="fejezetVizio" class="fejezet">
    <div class="container">
        <h1><?=$nyelv["vizio"]?></h1>
        <h2 class="fontBlue"><?=$nyelv["vizio_1_maintitle"]?></h2>
        <h2><?=$nyelv["vizio_1_subtitle"]?></h2>
        <div class="vizioKep vizioJobbra"><img src="image/vizioVizio.png" class="kepSzelesseg"></div>
        <div class="vizioSzoveg vizioBalra" id="vizioSzoveg" >
            <?=$nyelv["vizio_1_text"]?><br><br>
            <?=$nyelv["vizio_1_text_2"]?><br><br>
            <?=$nyelv["vizio_1_text_3"]?>
        </div>
        <div class="clear vizioTerkoz"></div>

        <h2 class="fontBlue"><?=$nyelv["vizio_2_maintitle"]?></h2>
        <h2><?=$nyelv["vizio_2_subtitle"]?></h2>
        <div class="vizioKep vizioBalra"><img src="image/vizioSerious.png" class="kepSzelesseg"></div>
        <div class="vizioSzoveg vizioJobbra" id="seriousSzoveg"><?=$nyelv["vizio_2_text"]?></div>
        <div class="clear vizioTerkoz"></div>

        <h2 class="fontBlue"><?=$nyelv["vizio_3_maintitle"]?></h2>
        <h2><?=$nyelv["vizio_3_subtitle"]?></h2>
        <div class="vizioKep vizioJobbra"><img src="image/vizioGeneracio.png" class="kepSzelesseg"></div>
        <div class="vizioSzoveg vizioBalra" id="generacioSzoveg"><?=$nyelv["vizio_3_text"]?></div>
        <div class="clear"></div>

        <h2 class="fontBlue"><?=$nyelv["vizio_4_maintitle"]?></h2>
        <div class="vizioKep vizioBalra"><img src="image/vizioMukodik.png" class="kepSzelesseg"></div>
        <div class="vizioSzoveg vizioJobbra" id="mukodesSzoveg"><?=$nyelv["vizio_4_text"]?></div>
        <div class="clear vizioTerkoz"></div>
    </div>
</div>
