<div id="menu" class="screen">
    <div class="container">
        <div class="menuKep"></div>
        <?php include 'menuItem.php'; ?>
    </div>

</div>

<div id="menu" class="mobile">
    <div class="menuKep"></div>
    <img src="image/menu_icon.png" id="menuIkon" />
    <div class="clear"></div>
    <?php include 'menuItem.php'; ?>
</div>
