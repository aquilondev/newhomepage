<?php

    function writeDetails($nyelv,$topic,$number){
        $gyujt="";
        for($i=0;$i<$number;$i++){
            $gyujt.="
                <div class='detailItem'>
                    <img src='image/slides/".$topic."/".($i+1).".jpg' class='picture'>
                    <div class='description'>".$nyelv[$topic][$i]."</div>
                </div>
            ";
        }
        return $gyujt;
    }

    function writeButton($nyelv){
        return "
            <div class='buttonContainer'>
                <div class='detailButton left'></div>
                <div class='detailButton right'></div>
                <div class='detailButtonBack'>".$nyelv["vissza"]."</div>
            </div>
        ";
    }

?>


<div class="reszletFedo" id="reszletTeleki">
    <div class="detailContainer" style="background-image: url(image/slides/teleki/teleki_bkg.jpg);">
        <?=writeButton($nyelv)?>

        <div class="detailWrap">
            <div class="detailItems" tema="teleki">
                <?=writeDetails($nyelv,"teleki",3)?>
            </div>
        </div>
    </div>
</div>

<div class="reszletFedo" id="reszletDialog">
    <div class="detailContainer" style="background-image: url(image/slides/teljesitmeny/teljesitmeny_bkg.jpg);">
        <?=writeButton($nyelv)?>
        <div class="detailWrap">
            <div class="detailItems" tema="teljesitmeny">
                <?=writeDetails($nyelv,"teljesitmeny",4)?>
            </div>
        </div>
    </div>
</div>

<div class="reszletFedo" id="reszletDISC_Dialog">
    <div class="detailContainer" style="background-image: url(image/slides/discdialog/discdialog_bkg.jpg);">
        <?=writeButton($nyelv)?>
        <div class="detailWrap">
            <div class="detailItems" tema="teleki">
                <?=writeDetails($nyelv,"discdialog",3)?>
            </div>
        </div>
    </div>
</div>

<div class="reszletFedo" id="reszletVela">
    <div class="detailContainer" style="background-image: url(image/slides/giorgio/giorgio_bkg.jpg);">
        <?=writeButton($nyelv)?>
        <div class="detailWrap">
            <div class="detailItems" tema="vela">
                <?=writeDetails($nyelv,"giorgio",5)?>
            </div>
        </div>
    </div>
</div>

<div class="reszletFedo" id="reszletOrient">
    <div class="detailContainer" style="background-image: url(image/slides/orient/orient_bkg.jpg);">
        <?=writeButton($nyelv)?>
        <div class="detailWrap">
            <div class="detailItems" tema="orient">
                <?=writeDetails($nyelv,"orient",5)?>
            </div>
        </div>
    </div>
</div>

<div class="reszletFedo" id="reszletDISC">
    <div class="detailContainer" style="background-image: url(image/slides/disc/disc_bkg.jpg);">
        <?=writeButton($nyelv)?>
        <div class="detailWrap">
            <div class="detailItems" tema="orient">
                <?=writeDetails($nyelv,"disc",4)?>
            </div>
        </div>
    </div>
</div>

<div class="reszletFedo" id="reszletProbarepules">
    <div class="detailContainer" style="background-image: url(image/slides/ballon/ballon_bkg.jpg);">
        <?=writeButton($nyelv)?>
        <div class="detailWrap">
            <div class="detailItems" tema="orient">
                <?=writeDetails($nyelv,"ballon",5)?>
            </div>
        </div>
    </div>
</div>
