<?php

if (isset($_GET["lang"]) && $_GET["lang"]!=""){
    include 'language/lang.'.$_GET["lang"].'.php';
}
else{
    include 'language/lang.hu.php';
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Aquilone</title>
<link href="css/wurBase.css" rel="stylesheet" type="text/css" />

<link href="css/rIndex.css" rel="stylesheet" type="text/css" />
<link href="css/rFont.css" rel="stylesheet" type="text/css" />
<link href="css/rTermek.css" rel="stylesheet" type="text/css" />
<link href="css/rVizio.css" rel="stylesheet" type="text/css" />
<link href="css/rBanner.css" rel="stylesheet" type="text/css" />
<link href="css/rMenu.css" rel="stylesheet" type="text/css" />
<link href="css/rStat.css" rel="stylesheet" type="text/css" />
<link href="css/rEgyedi.css" rel="stylesheet" type="text/css" />
<link href="css/rReszlet.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>




<script>

$(document).ready(function(){

    $("#checkSize").html("szelessege: "+$(window).width()+"px; fedoablak: "+$(".detailContainer").height());
    setSize();

    $(window).on('resize',function(){
        setSize();
    });

    $(".gombErdekel").click(function(){
        neve=$(this).parent().attr("id");
        $("#reszlet"+neve).fadeIn();
        $("body").css("overflow","hidden")
    });

    $(".detailButtonBack").click(function(){
        $(this).closest(".reszletFedo").fadeOut();
        $("body").css("overflow","auto")
    });

    $('#menuIkon').click(function(){
		$('.mobile .menuItems').toggle();
	});

    $(".menuitem").click(function() {
        var attr = $(this).attr('data-language');
        if (typeof attr !== typeof undefined && attr !== false) {
            window.open("index.php?lang="+attr,'_self')
        }
        else {
            var clickableId = $(this).attr('data-type-position');
            $('html, body').animate({
                scrollTop: $('#fejezet' + clickableId).offset().top
            }, 200);
        }
    });

    $('.detailButton.left').click(function(){
        $(this).parent().children(".detailButton.right").fadeIn();
        szelesseg=$(".detailItem").width();
        ujpoz=$(this).parent().next().children().css("left");
        $(this).parent().next().children().animate({"left":"+="+szelesseg+"px"}, "slow");
        if (ujpoz=='-'+szelesseg+'px'){
            $(this).fadeOut();
        }
    });
    $('.detailButton.right').click(function(){
       $(this).parent().children(".detailButton.left").fadeIn();
       szelesseg=$(".detailItem").width();
       ujpoz=$(this).parent().next().children().css("left");
       teljes=$(this).parent().next().children().width();
        //console.log(ujpoz);
        //console.log(szelesseg);
        //console.log(teljes);
        keres='-'+(teljes-(szelesseg*2))+'px';
        //console.log(keres);
       $(this).parent().next().children().animate({"left":"-="+szelesseg+"px"}, "slow");
        if (ujpoz==keres){
            $(this).fadeOut();
        }
    });

});


function setSize(){
    $(".detailItems").each(function() {
        $(this).css("width",($(this).children().length*$(".detailItem").width())+"px");
        $(".detailItems").css("left","0px");
        $(".detailButton.left").hide();
        $(".detailButton.right").show();
    });
}

</script>


</head>
<body>
<!--br><br>
<div id="checkSize" style="background: LemonChiffon; color: black;padding: 10px;"></div>


<br><br-->

<?php include 'views/menu.php'; ?>
<?php include 'views/banner.php'; ?>

<?php include 'views/vizio.php'; ?>

<?php include 'views/termekek.php'; ?>

<?php include 'views/egyedi_video.php'; ?>

<?php include 'views/stat.php'; ?>

<?php include 'views/kapcsolat.php'; ?>

<?php include 'views/reszlet.php'; ?>


</body>
</html>
