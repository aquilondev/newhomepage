var mailBody;

$(function () {

    var currentModule;
    var currentSlide;
    var countSlides;

    var szovegek = {};

    szovegek['ballon'] = {};
    szovegek['ballon'][1] = 'A teljesítménymenedzsmentet támogató programunkat egy kalandos hőlégballonos utazás analógiájára építettük. Földi és légi oktatáson veszünk részt. A földi oktatás (Léghajósok) az alapokat adja át, míg a légi oktatás (Próbarepülés) a TM beszélgetésekhez kapcsolódó készségfejlesztés.';
    szovegek['ballon'][2] = 'A tanulásban, azaz a „hőlégballonos felkészülésben” egy tapasztalt pilóta kísér. Minden nap megkér egy-egy feladat elvégzésére. Itt például a sikeresség külső tényezőiről és a saját erősségekről kell, hogy gondolkozz, és amit értékesnek találsz, azt szimbolikusan belehelyezheted a hőlégballon kosarába. Látni fogod azt is, mások milyen elemeket választottak a leggyakrabban.';
    szovegek['ballon'][3] = 'Néha egész rejtélyes helyekre kalandozunk. De csakis a jó ügy érdekében. A komfortzóna elhagyása, illetve játékelemekkel támogatott önreflexió pedig a tanulást segíti ☺';
    szovegek['ballon'][4] = 'A felkészülés során, minden jól megoldott feladatért egy, a léghajózáshoz nélkülözhetetlen eszközzel gyarapszol. A teljesítménymenedzsmenthez szükséges tudást pedig egy külön „jegyzetben” gyűjtöd, amit bármikor elővehetsz,  ha szükséges.  A jegyzet letölthető és tartalmazza az összes, a játékba beépített tudást, sűrített formában.';
    szovegek['ballon'][5] = 'És a végén, ha már mindent magtanultunk a teljesítménymenedzsmentről, akkor a magasba emelkedünk, hogy folytassuk a kalandot és az alapok után elsajátíthassuk a legfontosabb készségeket.';
    szovegek['ballon'][6] = 'A program során érintett témák:<br/><ul><li>A jó teljesítmény összetevői.</li><li>A teljesítmény iránya, fókusza és minősége.</li><li>A teljesítmény értékelő beszélgetés és forgatókönyve.</li><li>Szerepek és felelősségi körök a teljesítmény menedzsmentben.</li><li>A feladat és a kapcsolati oldal harmonikus működtetése a TM-ben.</li></ul>A program során felhalmozott eszköztár egyéb vezetői/munkatársi kommunikációs helyzetben is rendkívül hasznos lehet.<br/><br/>10+15 napban, napi 10-15 perc.<br/>Résztvevők száma korlátlan';

    szovegek['disc'] = {};
    szovegek['disc'][1] = 'A DISC viselkedéstípusok programunk DISC tréning utánkövetésként, vagy önálló tananyagként is kiválóan használható. A történet New Orleansban játszódik, ahová négy idegen érkezik a „DISC minden irányából”. Az ő helyzeteikkel, ügyes bajos dolgaikkal találkozol Te, mint szállásadó. A feladat nem mindig egyszerű, de izgalmas és hasznos. Tarts velünk!';
    szovegek['disc'][2] = 'Az első 4 napban a 4 viselkedés dimenziót elevenítjük meg neked, a domináns, a befolyásoló, a stabil és a szabálykövető viselkedések külső és nonverbális kommunikációs jegyeire építve. Egyedül itt sem maradsz, hiszen könyörtelen főnököd, Tim segít, ha elakadnál…';
    szovegek['disc'][3] = 'Megismerkedhetünk a vendégek fő viselkedési dimenzióival, kommunikációs és motivációs igényeivel, jellemzőivel.';
    szovegek['disc'][4] = 'A DISC viselkedéstípusok program során nemcsak gyakorlatok egymásutánjával találkozik a felhasználó, hanem egy teljes, részletes DISC kézikönyv is összeáll a háttérben. A program önálló modulként vagy<br/><ul><li>Kommunikáció</li><li>Együttműködés fejlesztés</li><li>Vezetésfejlesztés</li></ul>témákban szolgálhat kiegészítő modulként.<br/><br/>15 napban, napi 10-15 perc<br/>Résztvevők száma korlátlan';
    // szovegek['disc'][4] = 'Van. Hogy egy-egy mozifilmet hívunk segítségül, hogy az elsődleges és másodlagos viselkedési típus jegyeit  gyakoroljuk, mélyítsük.';

    szovegek['orient'] = {};
    szovegek['orient'][1] = 'Stresszkezelés programunkkal játékosaink Európa leghíresebb vasútján Párizstól Isztambulig utazva kapnak gyakorlati módszereket.';
    szovegek['orient'][2] = 'Tréfás és komoly, egyéni és csoportos feladatok várnak az utazókra.';
    szovegek['orient'][3] = 'A stresszcsökkentő és figyelemkoncentrációs gyakorlatok  a játékból egyszerűen átültethetőek a valódi életbe. Így buzdítjuk is a felhasználókat, hogy álljanak fel a gép elől és mozogjanak egy kicsit. Ez jó módszer egy hosszú vonatúton is.';
    szovegek['orient'][4] = 'Dolgozunk az objektív és a szubjektív idővel és tippeket adunk arra, hogy a saját időbeosztásunk hasznosan teljen. Tervezés-újratervezés-szervezés. ☺';
    szovegek['orient'][5] = 'Programunkat ajánljuk mindazoknak, akik nagy időnyomás alatt és az ebből fakadó komoly fizikai, szellemi és pszichikai terhelésnek vannak kitéve.<br/><br/>15 napban, napi 10-15 perc<br/>Résztvevők száma korlátlan';

    szovegek['giorgio'] = {};
    szovegek['giorgio'][1] = 'A „Giorgio Vela nyomában” vezetésfejlesztést támogató online program. A kaland során Marseilles-ből egészen egy eldugott Indiai-óceáni szigetre visz az utunk. Közben vezetői helyzetekben kell helytállnunk egy hajó kapitányaként.';
    szovegek['giorgio'][2] = 'A legénység néha bizony szem elől téveszti a célokat és az irányt. Ezért jó, ha van egy kapitány, aki megerősíti és összetartja a csapatot.';
    szovegek['giorgio'][3] = 'Nem elég azonban jó szakembernek lenni. Ismerni kell a munkatársak elvárásait, felismerni, megjegyzéseik, mondataik valójában miről szólnak... Az empátia és a kapcsolati oldal ápolása nélkülözhetetlen vezetői kompetencia. A figyelem és megfelelő értelmezés az alapja a támogató vezetői viselkedésnek.';
    szovegek['giorgio'][4] = 'Útközben segítőink is akadnak. Ilyen Thomas kapitány Colombóban, akinek a vezetői motivációs modellje megkérdőjelezhetetlen és a nyugati világban is hasznos módszer.';
    szovegek['giorgio'][5] = 'Vezetésfejlesztő programunkat ajánljuk teljes vezetésfejlesztéshez illetve rész modulokhoz.<br/>4X10 napos, napi 15 percet igénylő modul:<br/><ul><li>Vezetői önismeret: a transzformális  vezetés, autoritás, felelősség, bizalom, kitartás, vezetői teszt</li><li>A vezetés kapcsolat és változás oldala: EQ, kapcsolatmenedzsment kompetenciák, kommunikáció, visszajelzés, változás kezelése feladat-kapcsolat dimenziókra építve.</li><li>Feladat dimenzió: feladatok kezelése, delegálás, időmenedzsment, projekt szemlélet, számonkérés</li> <li>Csapatépítés: Csapatfejlődés fázisai és a vezető feladatai, bizalom, felelősség, eredményesség, konfliktusok kezelése, önmenedzselő munkatársak – motiváló környezet</li></ul>';

    szovegek['teljesitmeny'] = {};
    szovegek['teljesitmeny'][1] = 'Játékunk erősségekre alapuló, a TM feladat-kapcsolat-változás dimenzióit figyelembe vevő megközelítésre épül. A teljesítménydialógus modellezésben valós teljesítményt értékelő, követő és célkijelölő párbeszédben vesz részt a játékos vezetői vagy munkatársi szerepben. ';
    szovegek['teljesitmeny'][2] = 'Ahogy a beszélgetés halad, a játékos döntéseket hoz, és ezeknek a döntéseknek következménye van, a másik szereplő ezekre valós reakciókat ad. A helyes megoldásra egy mentor ösztönzi, illetve ad visszajelzést akkor is, ha nem a megfelelő irányba indul a párbeszéd.';
    szovegek['teljesitmeny'][3] = 'A legfontosabb tudnivalók letölthetők a játékból.';
    szovegek['teljesitmeny'][4] = 'Minden nap végén kérünk véleményt a játékosainktól.<br/><br/>A játék statisztikáit egy felületen a HR szakemberek részére hozzáférhetővé tesszük, így folyamatosan követhetik munkatársaik haladását.';

    szovegek['teleki'] = {};
    szovegek['teleki'][1] = 'Minden nap az expedíciós sátorban kezdődik és ér véget. Itt kapod meg a feladatot és itt tudod ellenőrizni a pontjaidat, kipihenni az aznapi próbatételeket és nem utolsó sorban itt követheted nyomon a haladásod.';
    szovegek['teleki'][2] = 'A napi feladatok próbára teszik az együttműködő készséged és új ötletekkel, tanácsokkal látnak el a kommunikációs  helyzetek és feladatmegoldások terén.';
    szovegek['teleki'][3] = 'A kaland során a 130 évvel ezelőtti Teleki expedíció útját járod be. Ez a kaland egyedül veszélyes. Csak a társaid segítségével jutsz előre és érsz el a célig.';

    szovegek['discdialog'] = {};
    szovegek['discdialog'][1] = 'A DISC párbeszéd szimulációt követően nagy biztonsággal mehetsz bele akár kényesebb kommunikációs helyzetekbe is.';
    szovegek['discdialog'][2] = 'A helyzetek több irányba ágaznak szét, de csak egy a helyes. Mentorunk azonban mindig készen áll, hogy segítsen megtalálni a megfelelő hangnemet, és a megfelelő stílust.';
    szovegek['discdialog'][3] = 'Gyere, játssz, gyakorolj, hogy komfortosabb és magabiztosabb legyél minden viselkedés stílussal történő kommunikáció során.';	
	
    $('#showDisc').click(function () {
        $('#door').fadeOut(500);
        $('.slides').fadeIn(500);
        initSlides('disc', 4);
    });

    $('#showOrient').click(function () {
        $('#door').fadeOut(500);
        $('.slides').fadeIn(500);
        initSlides('orient', 5);
    });

    $('#showGiorgio').click(function () {
        $('#door').fadeOut(500);
        $('.slides').fadeIn(500);
        initSlides('giorgio', 5);
    });

    $('#showTeljesitmeny').click(function () {
        $('#door').fadeOut(500);
        $('.slides').fadeIn(500);
        initSlides('teljesitmeny', 4);
    });

    $('#showBallon').click(function () {
        $('#door').fadeOut(500);
        $('.slides').fadeIn(500);
        initSlides('ballon', 6);
    });

    $('#showTeleki').click(function () {
        $('#door').fadeOut(500);
        $('.slides').fadeIn(500);
        initSlides('teleki', 3);
    });
	
    $('#showDiscDialog').click(function () {
        $('#door').fadeOut(500);
        $('.slides').fadeIn(500);
        initSlides('discdialog', 3);
    });
	
    $('#gotoDoor').click(function () {
        $('.slides').fadeOut(500);
        $('#door').fadeIn(500);
    });

    $('#back').click(function () {
        $('#finish').fadeOut(500);
        $('#door').fadeIn(500);
    });

    $("#door-next").click(function () {
        $('#door').fadeOut(500);
        $('#finish').fadeIn(500);
    });

    $('.rightButton').click(function () {
        nextSlide = currentSlide+1;
        $("#"+currentSlide).hide("slide",{ direction: "left" }, 600);
        $("#"+nextSlide).show("slide",{ direction: "right" }, 600);
        currentSlide = nextSlide;
        // if(currentSlide == countSlides) {
        //     $('#door-next').show();
        // }
        showHidePagers();
    });

    $('.door-item').on('click', function () {
        $('#door-next').show();
    });

    $( '.leftButton' ).click(function () {
        prevSlide = currentSlide-1;
        $("#"+currentSlide).hide("slide",{ direction: "right" }, 600);
        $("#"+prevSlide).show("slide",{ direction: "left" }, 600);
        currentSlide = prevSlide;
        showHidePagers();
    });

    function showHidePagers() {
        if(currentSlide == 1) {
            $('.leftButton').hide();
        } else {
            $('.leftButton').show();
        }
        if(currentSlide == countSlides) {
            $('.rightButton').hide();
        } else {
            $('.rightButton').show();
        }
    }

    function initSlides(slide, maxSlide) {
        $('.slides').removeClass(currentModule).addClass(slide);
        currentModule = slide;
        countSlides = maxSlide;
        generateSlides();
        currentSlide = 1;
        showHidePagers();
        $('#'+currentSlide).show();
    }

    function generateSlides() {
        var i;
        var slideContent = '';
        for(i = 1;i <= countSlides;i++) {
            slideContent += "<div class='slideContent' id='"+i+"'>" +
                "<div class='slideImage'><img src='slides/"+currentModule+"/"+i+".jpg' /></div>" +
                "<div class='slideText'>"+szovegek[currentModule][i]+"</div>" +
                "</div>";
        }
        $('.slideWrapper').html(slideContent);
    }

    var robi = $('.robi');
    var kutya = $('.kutya');

    var currentPage = 'landing';
    $('#survey-next.landing').on('click', function () {

        if (currentPage == 'landing') {

            currentPage = 'survey';

            $(this).removeClass('landing').addClass('thanks');

            $.post('./savepost.php', $( "#survey form" ).serialize() );

            robi.removeClass('balra-mutat').animate({'left' : (parseInt(robi.css('left'), 10) - 350) + 'px'});
            kutya.animate({'left' : (parseInt(kutya.css('left'), 10) - 200) + 'px'});

            $('#survey-desc').hide();

            $('#questions').fadeOut(200);
            $('#survey-thanks').delay(300).show().animate({left: '30px'}, 500);

        } else if (currentPage == 'survey') {

            currentPage = 'video';

            robi.addClass('balra-mutat').animate({'left' : (parseInt(robi.css('left'), 10) + 350) + 'px'});
            kutya.animate({'left' : (parseInt(kutya.css('left'), 10) + 200) + 'px'});

            $('#survey-thanks').fadeOut(300);
            $('#demo-video').fadeIn(300);

            setTimeout(function () {
                document.getElementById('demo-video-player').play();
            }, 1000);

        } else if (currentPage == 'video') {
			document.getElementById('demo-video-player').pause();
            $('#survey').fadeOut(500);
            $('#entrance').fadeIn(500);
        }

        return false;
    });

    $('#enter').on('click', function () {
        $('#entrance').fadeOut(500);
        $('#door').fadeIn(500);
    });

    var doorRight = $('#door-right');
    var doorContentWrapper = $('#door-content-wrapper');
    var doorLeft = $('#door-left');
	
	
    doorRight.on('click', function () {
		ideig=doorContentWrapper.css("left");
		if (ideig=="0px"){
			doorContentWrapper.animate({left: '-100%'}, 600);
			//doorRight.hide();
			doorLeft.show();
			pagerLeft.removeClass('active');
			pagerRight.addClass('active');
			pagerPlusz.removeClass('active');
		}
		else{
			doorContentWrapper.animate({left: '-200%'}, 600);
			doorRight.hide();
			doorLeft.show();
			pagerLeft.removeClass('active');
			pagerRight.removeClass('active');
			pagerPlusz.addClass('active');
		}
    });
    doorLeft.on('click', function () {
		ideig=doorContentWrapper.css("left");
		if (ideig=="-763px"){
			doorContentWrapper.animate({left: '0%'}, 600);
			doorRight.show();
			doorLeft.hide();
			pagerLeft.addClass('active');
			pagerRight.removeClass('active');
			pagerPlusz.removeClass('active');
		}
		else{
			doorContentWrapper.animate({left: '-100%'}, 600);
			doorRight.show();
			//doorLeft.hide();
			pagerLeft.removeClass('active');
			pagerRight.addClass('active');
			pagerPlusz.removeClass('active');
		}
    });

/*	
	
    var pagerLeft = $('#pager-left');
    var pagerRight = $('#pager-right');
    var pagerPlusz = $('#pager-plusz');
    pagerLeft.on('click', function () {
        pagerLeft.addClass('active');
        pagerRight.removeClass('active');
        pagerPlusz.removeClass('active');
        doorLeft.trigger('click');
    });
    pagerRight.on('click', function () {
        pagerLeft.removeClass('active');
        pagerRight.addClass('active');
        pagerPlusz.removeClass('active');
        doorRight.trigger('click');
    });
    pagerPlusz.on('click', function () {
        pagerLeft.removeClass('active');
        pagerRight.removeClass('active');
        pagerPlusz.addClass('active');
        doorPlusz.trigger('click');
    });
*/

    var pagerLeft = $('#pager-left');
    var pagerRight = $('#pager-right');
    var pagerPlusz = $('#pager-plusz');
    pagerLeft.on('click', function () {
        pagerLeft.addClass('active');
        pagerRight.removeClass('active');
        pagerPlusz.removeClass('active');
		doorRight.show();
		doorLeft.hide();
        doorContentWrapper.animate({left: '0%'}, 600);
    });
    pagerRight.on('click', function () {
        pagerLeft.removeClass('active');
        pagerRight.addClass('active');
        pagerPlusz.removeClass('active');
        doorContentWrapper.animate({left: '-100%'}, 600);
		doorRight.show();
		doorLeft.show();
    });
    pagerPlusz.on('click', function () {
        pagerLeft.removeClass('active');
        pagerRight.removeClass('active');
        pagerPlusz.addClass('active');
        doorContentWrapper.animate({left: '-200%'}, 600);
		doorRight.hide();
		doorLeft.show();
    });



    function preload(arrayOfImages) {
        $(arrayOfImages).each(function(){
            $('<img/>')[0].src = this;
            // Alternatively you could use:
            // (new Image()).src = this;
        });
    }

    // $('#interested').on('click', function () {
    //     $.post('./sendmail.php', $( "#finish-left form" ).serialize() );
    //     $('#interested').fadeOut(500);
    // });

    $('#interested').on('click', function () {
        generateEmail();
        //$('#interested').fadeOut(500);
        setTimeout(function(){
            $('#final-thanks').show();
        },1000);

    });


    preload(['slides/ballon/ballon_bkg.jpg', 'slides/disc/disc_info_bkg.jpg', 'slides/giorgio/giorgio_info_bkg.jpg', 'slides/teleki/teleki_bkg.jpg', 'slides/discdialog/discdialog_bkg.jpg',
    'slides/orient/orient_bkg.jpg', 'slides/teljesitmeny/teljesitmeny_bkg.jpg', 'images/belepes_gomb.png']);

});

function generateEmail() {

    mailBody = 'Tisztelt Aquilone Kft!<br><br>';

    mailBody += 'A következő játékokról szeretnék több információt kapni:';
    $('#finish-left input:checked + label h2').each(function () {
        mailBody += '<br> • ' + $(this).text().trim();
    });
	

    mailBody += '<br><br>A mi cégünknél ezek a jelenségek tapasztalhatók:';
    $('#questions input:checked + label:not(.other)').each(function () {
        mailBody += '<br> • ' + $(this).text().trim();
    });

    var textInput = $('#questions input[type=text]').val().trim();
    if (textInput.length >= 0) {
        mailBody += '<br><br>Ilyen egyéb jelenség fordul elő még: ' +  textInput;
    }

    mailBody += '<br><br>Üdvözlettel<br><br>';

    email=$("#email").val();

    $.ajax({url:"ajax/sendMail.php",
        type:"POST",
        data: {"email":email, "details":mailBody},
        success:function(result){
        }
    });



/*    window.location.href = 'mailto:dontreply@aquilone.hu?subject=Új érdeklődő a hírlevél oldalról&body='
        + encodeURIComponent(mailBody);*/
}