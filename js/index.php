<?php

$surveyOptions = [
    'Kevesebb idő van tréningre menni.',
    'A fejlesztések és tréningek nem támogatják közvetlenül és azonnal munkavégzést és nem következik be az elvárt viselkedésváltozás.',
    'Költséghatékonyabb programokra van szükség.',
    'Egyre nagyobb az igény, hogy a program inspiráljon és erősebben kötelezzen el a fejlődés mellett.',
    'Kevés a visszajelzés a programok sikerességéről.',
    'A résztvevők izgalmasabb, gyakorlatiasabb, dinamikusabb programra vágynak.',
    'Lényegesen több résztvevő bevonására van szükség egy időben, mint amit a tréning ki tud szolgálni.',
];

$doorContents = [
    [
        'probarepules_disc.png',
        'Próbarepülés',
        'Teljesítménymenedzsment',
        '<p>A <strong>„Léghajósok”</strong> és a <strong>„Próbarepülés</strong> teljesítménymenedzsmentet támogató online eszközök.</p> 
         <p>A TM folyamatot egy léghajós felkészítés keretében kísérjük végig. A ”Léghajósok” az adott TM alapjait rakja le (a konkrét vállalatra szabjuk).</p>
         <p>A „Próbarepülés” vezetői és munkatársi moduljai a célkijelölő, utánkövető és értékelő beszélgetésekhez – a TM működtetéséhez szükséges készségeket fejleszti.”</p>',
        'showBallon',
    ], [
        'disc_disc.png',
        'DISC',
        'Viselkedés típusok New Orleansban',
        '<p>New Orleansban kell jó kommunikációs készségünket bizonyítani egy kemény főnök keze alatt ebben a fejlesztő programban.
            A DISC viselkedési stílusok alkalmazásával azonban minden rázósnak tűnő helyzetet megoldhatunk!
            A játékból a DISC rendszerét nem ismerők elsajátíthatják a tudást, aki meg már tréningen is részt vett, nekik segít a beépülésben, alkalmazásban. 
            (már angolul is!)</p>',
        'showDisc'
    ], [
        'orient_disc.png',
        'Orient Expressz',
        'Stresszmenedzsment',
        '<p>A legendás Orient Express kiváló helyszíne egy stresszmentesítő utazásnak. Városról városra utazva a csodás kocsi kényelmében 
            gyakorlati módszereket tanulhatunk a stressz kezeléséhez. Persze, ez a világ sem tökéletes, valami furcsa titok lengi be ezt a kalandos utazást...</p>',
        'showOrient'
    ], [
        'giorgio_disc.png',
        'Giorgo Vela nyomában',
        'Vezetőknek',
        '<p>A <strong>„Giorgo Vela  nyomában”</strong> programban a vezetés legfontosabb dimenzióival dolgozunk.</p>
         <p>A négy önállóan is alkalmazható modul: Vezetői önismeret; A vezetés kapcsolati dimenziója; A vezetés feladat oldala; Csapatfejlesztés.</p>
         <p>Ezt a programot már több száz, különböző szintű vezető játszotta.  Izgalmasnak, nagyon hasznosnak találták.</p>',
        'showGiorgio'
    ], [
        'dialogus_disc.png',
        'Teljesítmény Dialógus',
        'Modellezés',
        '<p>A <strong>„Teljesítménydialógus modellezésben”</strong> konkrét teljesítmény- értékelő, követő és célkijelölő párbeszédben vesz részt a játékos munkatársi vagy vezetői szerepben.</p> 
         <p>Ebben a szimulációban döntései befolyásolják a beszélgetés sikerességét, a megoldásban, megértésben Mentor segíti.</p>',
        'showTeljesitmeny'
    ], [
        'teleki_disc.png',
        'Csoportos együttműködés ',
        'fejlesztés a Teleki expedíció nyomában',
        '<p>Az együttműködés szimulációs játékunk ugyanúgy online eszköz, mégis a csoportos munkát és a csapatmechanizmusokat erősíti. Afrikában számos kaland közepette modellezünk valós kihívásokat, melyek kiválóan átültethetőek a napi gyakorlatba.</p>',
        'showTeleki'
    ], [
        'szituacio_disc.png',
        'Párbeszéd szimulációk',
        'DISC alapon',
        '<p>A DISC gyakorló szituációk a már meglévő DISC stílus ismeretekre építve nyújt gyakorlási lehetőséget konkrét szituációkra, kommunikációs helyzetekre.</p>',
        'showDiscDialog'
    ]
];

$finishContent = [
    [
        'probarepules_disc.png',
        'PRÓBAREPÜLÉS',
        'Teljesítménymenedzsment',
        'probarepules',
    ], [
        'disc_disc.png',
        'DISC',
        'Viselkedéstípusok New Orleansban',
        'disc',
    ], [
        'orient_disc.png',
        'Orient Express',
        'Időgazdálkodás és stresszmenedzsment',
        'orient',
    ], [
        'giorgio_disc.png',
        'Giorgo Vela nyomában',
        'Vezetésfejlesztés',
        'giorgo',
    ], [
        'dialogus_disc.png',
        'teljesítménydialógus modellezés',
        'Hatékonyabb teljesítmény beszélgetés',
        'dialogus',
    ], [
        'teleki_disc.png',
        'Csoportos együttműködés',
        'fejlesztés a Teleki expedíció nyomában',
        'teleki',
    ], [
        'szituacio_disc.png',
        'Párbeszéd szimulációk',
        'DISC alapon',
        'dialogusdisc',
    ]
];

?>
<!DOCTYPE html>
<html lang="hu">
    <head>
        <meta charset="UTF-8">
        <title>Hírlevél</title>

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

        <link rel="stylesheet" type="text/css" href="./styles_v1.css" />
        <script type="text/javascript" src="./scripts_v3.js"></script>

        <link rel="icon" href="favicon.png">
    </head>
    <body>
        <div id="wrapper">
            <div id="container" class="survey">
                <div id="aquilone-logo"></div>

                <div id="survey" class="main-content">
                    <div class="robi balra-mutat"></div>
                    <div class="kutya"></div>
                    <div id="questions">
                        <form>
                            <h1>Mit tapasztal Ön az alábbi jelenségek közül?</h1>
                                <?php for ($i=1; $i<=count($surveyOptions); $i++) { ?>
                                    <input type="hidden" name="survey[<?= $i ?>]" value="0"/>
                                    <input type="checkbox" name="survey[<?= $i ?>]" id="survey[<?= $i ?>]" value="1"/>
                                    <label for="survey[<?= $i ?>]">
                                        <?= $surveyOptions[$i-1] ?>
                                    </label>
                                <?php } ?>
                            <input type="hidden" name="survey[<?= count($surveyOptions) + 1 ?>]" value="0"/>
                            <input type="hidden" id="email" name="email" value="<?= $_REQUEST['email']?>" />
                            <input type="checkbox" name="survey[<?= count($surveyOptions) + 1 ?>]" id="survey[<?= count($surveyOptions) + 1 ?>]" value="1"/>
                            <label for="survey[<?= count($surveyOptions) + 1 ?>]" class="other">
                                Egyéb tapasztalat: <input type="text" class="other" name="survey[<?= count($surveyOptions) + 2 ?>]" />
                            </label>
                        </form>
                    </div>
                    <div id="survey-desc">
                        <p>A tapasztalt jelenséget kérem, kattintással jelölje! </p>
                    </div>
                    <div id="survey-thanks">
                        <p>Nagyon köszönöm válaszát!</p>
                        <p>Dávid Róbert vagyok, az Aquilone Training Kft. Gamification tanácsadója.</p>
                        <p>Tartson velem és bemutatom, hogy milyen szervezeti gamification megoldásokkal támogatjuk partnereinket, fejlesztéseinket!</p>
                    </div>

                    <div id="demo-video">
                        <video id="demo-video-player" class="video" controls>
                            <source src="Aquilone Gamification.mp4" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                    </div>

                    <a class="button landing" id="survey-next" href="#">Tovább</a>

                </div>

                <div id="entrance" class="main-content">
                    <a id="enter" href="#"></a>
                </div>

                <div id="door" class="main-content">

                    <a href="#" id="door-left"></a>
                    <a href="#" id="door-right"></a>

                    <div id="door-wrapper">

                        <div id="door-title">
                            <h1>Üdvözöljük az Aquilone Training játékosított tanulásában!</h1>
                        </div>


                        <div id="door-content">
                            <div id="door-content-wrapper">
                                <?php for($i=0; $i<count($doorContents); $i++) {
                                        $content = $doorContents[$i];
                                ?>
                                    <a href="#" class="door-item" id="<?= $content[4] ?>">
                                        <div class="image" style="background-image: url('./images/<?= $content[0] ?>')"></div>
                                        <h2><?= $content[1] ?></h2>
                                        <h3><?= $content[2] ?></h3>
                                        <div class="desc"><?= $content[3] ?></div>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>

                    </div>

                    <div id="pager">
                        <a href="#" id="pager-left" class="active"></a>
                        <a href="#" id="pager-right"></a>
                        <a href="#" id="pager-plusz"></a>
                    </div>

                    <a class="button door" id="door-next" href="#">Tovább</a>

                </div>

                <div class="slides main-content">
                    <div class="leftButton"><a href="#"></a></div>
                    <div class="rightButton"><a href="#"></a></div>
                    <div class="slideWrapper">
                        <div class="slideContent">
                            <div class="slideImage"><img src="slides/ballon/1.jpg" /></div>
                            <div class="slideText"></div>
                        </div>
                    </div>
                    <a class="button" id="gotoDoor" href="#">Vissza</a>
                </div>

                <div id="finish" class="main-content">
                    <div id="finish-wrapper">
                        <div id="finish-left">
                            <div id="finish-arrow"></div>
                            <h1>Kattintson arra a fejlesztő programra, ami felkeltette érdeklődését, és kollégánk megkeresi Önt!</h1>
                            <form>
                                <?php foreach ($finishContent as $item) { ?>
                                    <div class="finish-content">
                                        <input type="checkbox" name="interest[]" value="<?= $item[1] ?>" id="<?= $item[3] ?>">
                                        <label class="finish-item" id="<?= $item[3] ?>" for="<?= $item[3] ?>">
                                            <img src="./images/<?= $item[0] ?>" />
                                                <h2><?= $item[1] ?></h2>
                                                <p><?= $item[2] ?></p>
                                        </label>
                                    </div>
                                <?php } ?>
                                <input type="hidden" name="email" value="<?= $_REQUEST['email'] ?>" />
                            </form>
                        </div>
                        <div id="finish-right">
                            <div id="finish-thanks">
                                <p>Köszönjük a figyelmet!</p>
                                <p>Ha felkeltettük érdeklődését, kérem, hogy keressen bennünket az alábbi elérhetőségeinken.</p>
                            </div>
                            <div id="company-details">
                                <img id="logo-finish" src="images/aquilone_logo_kek.png">
                                <div>
                                    <p class="highlight">AQUILONE TRAINING KFT.</p>
                                    <a href="https://www.google.hu/maps/place/Budapest,+Amerikai+%C3%BAt+76,+1145/@47.517813,19.0955209,17z/data=!3m1!4b1!4m5!3m4!1s0x4741db78e00977bb:0x8f50a8405b48600d!8m2!3d47.517813!4d19.0977096" target="_blank">1145 Budapest, Amerikai út 76.</a>
                                </div>
                                <div id="robert-finish">
                                    <img id="robi-profilkep" src="images/robi_profilkep.png">
                                    <p class="highlight">Dávid Róbert</p>
                                    <a href="mailto:david.robert@aquilone.hu?subject=Érdeklődés szervezeti gamification témakörben&body=Kedves Dávid Róbert!
                        %0D%0A%0D%0ASzívesen látnék bővebb információt munkáikról, tapasztalataikról, eredményeikről szervezeti gamification témában.
                        %0D%0A%0D%0AÜdvözlettel">david.robert@aquilone.hu</a>
                                    <a href="tel:+36704282360">Tel: 06 70 428 2360</a>
                                    <a href="http://www.aquilone.hu" target="_blank">www.aquilone.hu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="robi-finish"></div>
                    <a href="#" id="interested" class="button green">Érdekel a játék!</a>
                    <a href="#" id="back" class="button">Vissza</a>
                    <a href="http://www.aquilone.hu" id="exit" class="button">Aquilone.hu</a>
                </div>
            </div>


            <div id="final-thanks">
                <div id="final-thanks-content">		
					Köszönjük érdeklődését!<br/><br/>
					Kollégáink hamarosan felveszik Önnel a kapcsolatot.<br/><br/> 
					Addig is szeretnénk megajándékozni a <a href="http://game.aquilone.hu/hirlevel_2016/johari/hirlevJohariIndex.php" target="_blank" style="color: #5084c3"><b><u><i>Megerősítések tükre</i></u></b></a> nevű alkalmazásunkkal. Ebben a játékban Johari ablak szerűen különböző erősségeink meglétéről kérhetünk visszajelzést a környezetünktől, azaz általunk felkért ismerőseinktől.<br /><br />
					Köszönettel:<br /><br />
					Az Aquilone Training csapata<br /><br />
					<div id="closeButton" onclick="$('#final-thanks').fadeOut(500)"></div>
                </div>
            </div>

        </div>
    </body>
</html>